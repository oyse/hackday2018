
import math
from time import sleep

from hackday.acgymanager import AcGyManager

ag = AcGyManager(offsetfile='/home/pi/hackdaydata/ag-offsets')

print 'Starting monitor mode. Press Ctrl-C to exit.'
ag.start()
while True:
    data = ag.get_data()

    print 'Accelerometer:'
    print '  x: %f' % round(data.accl_x, 2)
    print '  y: %f' % round(data.accl_y, 2)
    print '  z: %f' % round(data.accl_z, 2)
    print 'Gyro:'
    print '  x: %f' % round(data.gyro_x, 2)
    print '  y: %f' % round(data.gyro_y, 2)
    print '  z: %f' % round(data.gyro_z, 2)
    print ''
    print '--------------------'

    sleep(0.2)
