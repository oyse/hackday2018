
from time import sleep

from hackday.acgymanager import AcGyManager

ag = AcGyManager(offsetfile='/home/pi/hackdaydata/ag-offsets')

print 'Calibrating ...'
ag.calibrate(iter_duration=30.0, iter_step=0.5, max_iter=10)

ag.print_offsets()

print ''
print 'Done calibrating!'

