
import math
import os
import pickle
from collections import namedtuple
from threading import Event, Thread
from time import sleep

import smbus


def read_byte(bus, address, reg):
    return bus.read_byte_data(address, reg)


def read_word(bus, address, reg):
    h = bus.read_byte_data(address, reg)
    l = bus.read_byte_data(address, reg+1)
    value = (h << 8) + l
    return value


def read_word_2c(bus, address, reg):
    val = read_word(bus, address, reg)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val


def dist(a,b):
    return math.sqrt((a*a)+(b*b))


def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)


def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)


def update_sensor_offset(measurement_sum, count, offset):
    return offset + float(measurement_sum)/count


AcGyData = namedtuple('AcGyData', ['accl_x', 'accl_y', 'accl_z',
                                   'gyro_x', 'gyro_y', 'gyro_z', ])


class AcGyManager:
    def __init__(self, offsetfile, monitor_pause=0.1, i2c_address=0x68):
        self._exit_event = None
        self._data = None

        self._offsetfile = offsetfile

        self._bus = smbus.SMBus(1)
        self._power_mgmt_1 = 0x6b
        self._power_mgmt_2 = 0x6c
        self._i2c_address = i2c_address

        self._monitor_pause = monitor_pause

        self.gyro_x_offset, self.gyro_y_offset, self.gyro_z_offset = 0, 0, 0
        self.accl_x_offset, self.accl_y_offset, self.accl_z_offset = 0, 0, 0
        self.load_offset()

    def get_data(self):
        if self._exit_event is None:
            raise RuntimeError('Please start the manager before requesting data')
        return self._data

    def start(self):
        t = Thread(target=self, name='AcGyManager')
        t.daemon = True
        t.start()

    def close(self):
        if self._exit_event is None:
            raise RuntimeError('Manager is not running.')
        self.exit_event.set()

    def __call__(self):
        self._data = AcGyData(0, 0, 0, 0, 0, 0)
        self._exit_event = Event()

        while not self._exit_event.is_set():
            accl_x = read_word_2c(self._bus, self._i2c_address, 0x3b) - self.accl_x_offset
            accl_y = read_word_2c(self._bus, self._i2c_address, 0x3d) - self.accl_y_offset
            accl_z = read_word_2c(self._bus, self._i2c_address, 0x3f) - self.accl_z_offset

            gyro_x = read_word_2c(self._bus, self._i2c_address, 0x43) - self.gyro_x_offset
            gyro_y = read_word_2c(self._bus, self._i2c_address, 0x45) - self.gyro_y_offset
            gyro_z = read_word_2c(self._bus, self._i2c_address, 0x47) - self.gyro_z_offset

            self._data = AcGyData(
                    accl_x=accl_x / 16384.0,
                    accl_y=accl_y / 16384.0,
                    accl_z=accl_z / 16384.0,
                    gyro_x=gyro_x / 131.0,
                    gyro_y=gyro_y / 131.0,
                    gyro_z=gyro_z / 131.0,
                )

            sleep(self._monitor_pause)

        self._data = None
        self._exit_event = None

    def calibrate(self, iter_duration, iter_step, max_iter):
        """
        Args:
            iter_duration (float): Time in seconds for each calibration iteration.
            iter_step (float): Time in seconds between each measurment.
            max_iter (int): Maximum number of calibration iterations.
        """
        # activate module
        self._bus.write_byte_data(self._i2c_address, self._power_mgmt_1, 0)

        iter_no = 0
        count, iter_time = 0, 0.0
        gyro_x_sum, gyro_y_sum, gyro_z_sum = 0, 0, 0
        accl_x_sum, accl_y_sum, accl_z_sum = 0, 0, 0

        gyro_x_offset, gyro_y_offset, gyro_z_offset = self.accl_x_offset, self.accl_y_offset, self.accl_z_offset
        accl_x_offset, accl_y_offset, accl_z_offset = self.gyro_x_offset, self.gyro_y_offset, self.gyro_z_offset

        while iter_no < max_iter:
            print 'Iteration #%d ... (%f / %f)' % (iter_no+1, iter_time, iter_duration)

            if iter_time >= iter_duration:
                # update gyro offsets
                gyro_x_offset = update_sensor_offset(gyro_x_sum, count, gyro_x_offset)
                gyro_y_offset = update_sensor_offset(gyro_y_sum, count, gyro_y_offset)
                gyro_z_offset = update_sensor_offset(gyro_z_sum, count, gyro_z_offset)

                # update acceleration offsets
                accl_x_offset = update_sensor_offset(accl_x_sum, count, accl_x_offset)
                accl_y_offset = update_sensor_offset(accl_y_sum, count, accl_y_offset)
                accl_z_offset = update_sensor_offset(accl_z_sum, count, accl_z_offset)

                # reset
                count, iter_time = 0, 0.0
                gyro_x_sum, gyro_y_sum, gyro_z_sum = 0, 0, 0
                accl_x_sum, accl_y_sum, accl_z_sum = 0, 0, 0

                iter_no += 1

            gyro_x_sum += read_word_2c(self._bus, self._i2c_address, 0x43) - gyro_x_offset
            gyro_y_sum += read_word_2c(self._bus, self._i2c_address, 0x45) - gyro_y_offset
            gyro_z_sum += read_word_2c(self._bus, self._i2c_address, 0x47) - gyro_z_offset

            accl_x_sum += read_word_2c(self._bus, self._i2c_address, 0x3b) - accl_x_offset
            accl_y_sum += read_word_2c(self._bus, self._i2c_address, 0x3d) - accl_y_offset
            accl_z_sum += read_word_2c(self._bus, self._i2c_address, 0x3f) - accl_z_offset

            sleep(iter_step)
            iter_time += iter_step
            count += 1

        # store offsets
        self.accl_x_offset, self.accl_y_offset, self.accl_z_offset = accl_x_offset, accl_y_offset, accl_z_offset
        self.gyro_x_offset, self.gyro_y_offset, self.gyro_z_offset = gyro_x_offset, gyro_y_offset, gyro_z_offset
        self.store_offset()

    def store_offset(self):
        with open(self._offsetfile, 'wb') as outfile:
            pickle.dump(dict(
                    accl_x=self.accl_x_offset, accl_y=self.accl_y_offset, accl_z=self.accl_z_offset,
                    gyro_x=self.gyro_x_offset, gyro_y=self.gyro_y_offset, gyro_z=self.gyro_z_offset,
                ), outfile)

    def load_offset(self):
        if not os.path.exists(self._offsetfile):
            return

        with open(self._offsetfile, 'rb') as infile:
            of = pickle.load(infile)
        self.accl_x_offset, self.accl_y_offset, self.accl_z_offset = of['accl_x'], of['accl_y'], of['accl_z']
        self.gyro_x_offset, self.gyro_y_offset, self.gyro_z_offset = of['gyro_x'], of['gyro_y'], of['gyro_z']

    def print_offsets(self):
        print 'Accelerometer offsets:'
        print '  x: %f' % self.accl_x_offset
        print '  y: %f' % self.accl_y_offset
        print '  z: %f' % self.accl_z_offset
        print ''
        print 'Gyro offsets:'
        print '  x: %f' % self.gyro_x_offset
        print '  y: %f' % self.gyro_y_offset
        print '  z: %f' % self.gyro_z_offset

